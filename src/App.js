import React from 'react';
import './App.css';
import Home from "./pages/usr/Home";
import Login from "./pages/Login";
import PostCompany from "./pages/admin/PostCompany";
import { BrowserRouter as Router, Route } from "react-router-dom";
import CompanyProfile from './pages/admin/CompanyProfile';
import PutCompany from './pages/admin/PutCompany';

function App() {
  return ( 
    <div className ="App">
      <Router>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/company-profile">
            <CompanyProfile />
          </Route>
          <Route exact path="/company-profile/create">
            <PostCompany />
          </Route>
          <Route exact path="/company-profile/update/:id" component={PutCompany} />
      </Router>
    </div>
  );
}

export default App;
