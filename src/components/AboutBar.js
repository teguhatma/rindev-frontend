import React from 'react'
import { Container, Row, Col } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./AboutBar.css";

function AboutBar(){
    return (
        <Container className="about my-5">
            <h2>
                <strong>ABOUT US</strong>
            </h2>
            <Row>
                <Col md>
                    <p className="about__title"><strong>Lorem ipsum dolor sit amet</strong></p>
                    <p>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </Col>
                <Col>
                </Col>
            </Row>

        </Container>
    )
}

export default AboutBar;

