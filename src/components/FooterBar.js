import React, { Component } from 'react'
import { Container, Col, Row } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./FooterBar.css"

class FooterBar extends Component {
    constructor(props){
        super(props)

        this.state = {
            error: null,
            isLoaded: false,
            data: []
        }
    }

    componentDidMount() {
        fetch("http://127.0.0.1:5000/api/company-profile/")
        .then(response => response.json())
        .then((result) => {
            this.setState({
                isLoaded: true,
                data: result[0]
            });
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            })
        }
        );
    }

    render() {
        const { error, isLoaded, data } = this.state;

        if (error){
            return <div>Error: {error.message}</div>
        } else if (!isLoaded){
            return <div>Loading ...</div>
        } else if (data == null){
            return <div>Data Kosong</div>
        } else {
            return (
            <footer>
                <div className="footer__top py-4">
                    <Container>
                        <Row>
                            <Col md>
                                <h3 className="footer__top__title">{data.name}</h3>
                                <div className="footer__top__description">
                                    <p>{ data.address }</p>
                                    <p>{ data.province }</p>
                                    <p>{ data.country }</p>
                                    <br></br>
                                    <p><strong>Phone: </strong>{data.phone_number}</p>
                                    <p><strong>Email: </strong>{data.email}</p>
                                </div>
                            </Col>
                            <Col md>
                                <h3>Useful Links</h3>
                            </Col>
                            <Col md>
                                <h3>Join Our Newsletter</h3>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className="footer__bottom">
                    <Container>
                        <Row>
                            <Col md className="float-left py-2">
                                <p className="footer__bottom__p">© Copyright <strong>{data.name}</strong>. All Rights Reserved.</p>
                                <p className="footer__bottom__p">Design by <a href="#">SasakDeveloper</a></p>
                            </Col>
                            <Col md className="text-center py-2">
                                <p className="footer__bottom__p">Button Follow Here</p>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </footer>
            )
        }
    }
}

export default FooterBar;
