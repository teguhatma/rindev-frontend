import React, { Component } from 'react';
import { Jumbotron, Button, Container } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

class JumbotronBar extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: {
                "title": "Hello, World!",
                "description": "This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information."
            }
        }
    }
    render() {
        const { data } = this.state;
        return (
            <Jumbotron className="my-0">
                <Container>
                    <h1>{ data.title }</h1>
                    <p>
                        { data.description }
                    </p>
                    <p>
                        <Button variant="primary">Learn more</Button>
                    </p>
                </Container>
            </Jumbotron>
        )
    }
}

export default JumbotronBar;
