import React, { Component } from 'react'
import { Navbar, Nav, Container } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink } from "react-router-dom";
import "./NavigationBar.css";

class NavigationBar extends Component {
    render() {
        return (
            <Navbar className="navbar">
                <Container>
                    <Nav className="mr-auto">
                        <NavLink className="navbar__navbarLink" to="/">Home</NavLink>
                        <NavLink className="navbar__navbarLink" to="/login">About</NavLink>
                        <NavLink className="navbar__navbarLink" to="/">Contact</NavLink>
                        <NavLink className="navbar__navbarLink" to="/company-profile">Profile</NavLink>
                        <NavLink className="navbar__navbarLink" to="/">Login</NavLink>
                    </Nav>
                </Container>
            </Navbar>
        )
    }
}

export default NavigationBar;
