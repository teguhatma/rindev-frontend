import React from 'react'
import { ListGroup } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./SideBar.css";
import { Link } from "react-router-dom";

function SideBar() {
    return (
        <div className="my-5">
             <ListGroup className="my-4" defaultActiveKey="#link1">
                <ListGroup.Item action href="#link1">
                Dashboard
                </ListGroup.Item>
            </ListGroup>
            <ListGroup className="my-4">
                <Link to="/company-profile">
                    <ListGroup.Item action>
                        Company Profile
                    </ListGroup.Item>
                </Link>
                <ListGroup.Item action href="#link2">
                Link 2
                </ListGroup.Item>
                <ListGroup.Item action>
                This one is a button
                </ListGroup.Item>
            </ListGroup>
            <ListGroup className="my-4">
                <ListGroup.Item action href="#link1">
                Link 1
                </ListGroup.Item>
                <ListGroup.Item action href="#link2">
                Link 2
                </ListGroup.Item>
                <ListGroup.Item action>
                This one is a button
                </ListGroup.Item>
            </ListGroup>
        </div>
    )
}

export default SideBar;
