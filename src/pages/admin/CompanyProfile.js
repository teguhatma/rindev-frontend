import React, { Component } from 'react';
import { Container, Table, Row, Col } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from "react-router-dom";
import SideBar from '../../components/SideBar';
import "./CompanyProfile.css"

class CompanyProfile extends Component {
    constructor(props){
        super(props)

        this.state = {
            isLoaded: false,
            error: null,
            data: []
        }
    }
    componentDidMount(){
        fetch("http://localhost:5000/api/company-profile", {
            method: "GET"
        })
        .then(response => response.json())
        .then(result => {
            this.setState({
                data: result,
                isLoaded: true
            });
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            })
        }
        )
    }
    render() {
        const { error, isLoaded, data } = this.state;

        if (error){
            return <div>Error: {error.message}</div>
        }else if (!isLoaded){
            return <div>Loading ...</div>
        }else if (data == null){
            return <div>Data Kosong</div>
        }else {
            return (
                <Container fluid>
                    <Row>
                        <Col md={3}>
                            <SideBar />
                        </Col>
                        <Col md className="my-5">     
                            <Link className="btn mb-4 btn-primary" to="/company-profile/create">Create</Link>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                { data.map(item => 
                                    <tr key={ item.id }>
                                        <td>{ item.id }</td>
                                        <td>
                                            <Link to={{
                                                pathname: `/company-profile/update/${item.id}`,
                                                data: item }}>{ item.name }</Link>
                                        </td>
                                        <td>{ item.email}</td>
                                        <td>{ item.phone_number }</td>
                                    </tr>                       
                                    )}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Container>
            )
        }
    }
}

export default CompanyProfile;
