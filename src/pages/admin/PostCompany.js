import React, { Component } from 'react'
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import SideBar from '../../components/SideBar';
import "./PostCompany.css"

class PostCompany extends Component {
    constructor(props){
        super(props)

        this.state = {
            name: "",
            address: "",
            province: "",
            postal_code: "",
            country: "",
            email: "",
            website: "",
            phone_number: "",
        }
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange(evt, field){
        this.setState({[field]: evt.target.value})
    }

    handleSubmit = (event) => {
        fetch("http://127.0.0.1:5000/api/company-profile/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        })
        .then(response => {
            console.log(response)
            return response.json();
        });
        event.preventDefault();
    }

    render() {
        return (
            <Container fluid>
                <Row>
                    <Col md={3}>
                        <SideBar />
                    </Col>
                    <Col md className="my-5">   
                        <div className="postCompany__title">
                            <h1>Create New Company Profile</h1>
                        </div>   
                        <div className="postCompany__form">
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control type="text" name="name" placeholder="Enter name" value={this.state.name} onChange={(event)=>this.handleInputChange(event, "name")} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control type="email" name="email" placeholder="Enter Email" value={this.state.email} onChange={(event)=>this.handleInputChange(event, "email")} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Phone Number</Form.Label>
                                    <Form.Control type="text" name="phone_number" placeholder="Enter phone number" value={this.state.phone_number} onChange={(event)=>this.handleInputChange(event, "phone_number")}/>
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Website</Form.Label>
                                    <Form.Control type="text" name="website" placeholder="Enter website" value={this.state.website} onChange={(event)=>this.handleInputChange(event, "website")} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Province</Form.Label>
                                    <Form.Control name="province" type="text" placeholder="Enter Province" value={this.state.province} onChange={(event)=>this.handleInputChange(event, "province")} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Country</Form.Label>
                                    <Form.Control name="country" type="text" placeholder="Enter country" value={this.state.country} onChange={(event)=>this.handleInputChange(event, "country")} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Postal Code</Form.Label>
                                    <Form.Control name="postal_code" type="text" placeholder="Enter postal code" value={this.state.postal_code} onChange={(event)=>this.handleInputChange(event, "postal_code")} />
                                </Form.Group>

                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Address</Form.Label>
                                    <Form.Control name="address" type="text" placeholder="Enter Address" value={this.state.address} onChange={(event)=>this.handleInputChange(event, "address")} />
                                </Form.Group>

                                <Button variant="primary" type="submit">
                                        Submit
                                </Button>
                            </Form>
                        </div>  
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default PostCompany;
