import React, { Component } from 'react'

class PutCompany extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: props.location.data,
            dataGet: [],
            error: null,
            isLoaded: false
        }
    }

    componentDidMount(){
        fetch(`http://localhost:5000/api/company-profile/${this.state.data.id}`)
        .then(res => res.json())
        .then(result => {
            this.setState({
                dataGet: result,
                isLoaded: true
            })
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            })
        }
        )
    }
    render() {
        const { error, isLoaded, dataGet } = this.state;
        if (error){
            return <div>{error.message}</div>
        }else if (!isLoaded){
            return <div>Loading ... </div>
        } else {
            return (
                <div>
                    { dataGet.name}
                </div>
            )
        }
    }
}

export default PutCompany;
