import React from 'react'
import NavigationBar from "../../components/NavigationBar";
import JumbotronBar from "../../components/JumbotronBar";
import FooterBar from "../../components/FooterBar";
import AboutBar from "../../components/AboutBar";

function Home() {
    return (
        <div>
            <JumbotronBar />
            <NavigationBar />
            <AboutBar/>
            <FooterBar />
        </div>
    )
}

export default Home;
